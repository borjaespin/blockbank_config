// Definición de dependencias
const winston = require('winston');
const appRoot = require('app-root-path');


// Personalizamos para cada transporte de salida (fichero y consola)
var options = {
  file: {
    level: 'info',
    filename: `${appRoot}/logs/blockbank.log`,
    handleExceptions: true,
    json: true,
    maxsize: 1048576, // 1MB
    maxFiles: 1,
    colorize: false
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};



// Instancia de objeto con la configuración definida
var logger = new winston.Logger({
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console)
  ],
  exitOnError: false
});



// Exports de funciones
module.exports = logger;
